const data = require('gulp-data');
const del = require('del');
const fs = require('fs');
const gm = require('gulp-gm')
const gulp = require('gulp');
const gulpPug = require('gulp-pug');
const imagemin = require('gulp-imagemin');
const path = require('path');
const pug = require('pug');
const pump = require('pump');
const ts = require('gulp-typescript');

// Paths
const BUILD_DIR = 'public'
const CSS_SOURCE_PATHS = ['source/**/*.css'];
const CSS_WATCH_PATHS = CSS_SOURCE_PATHS;
const JPG_SOURCE_PATHS = ['source/**/*.jpg'];
const JPG_WATCH_PATHS = JPG_SOURCE_PATHS;
const PNG_SOURCE_PATHS = ['source/**/*.png'];
const PNG_WATCH_PATHS = PNG_SOURCE_PATHS;
const TYPESCRIPT_SOURCE_PATHS = ['source/**/*.ts'];
const TYPESCRIPT_WATCH_PATHS = TYPESCRIPT_SOURCE_PATHS;
const PUG_SOURCE_PATHS = [
    'source/**/*.pug',
    '!**/_*'
];
const PUG_WATCH_PATHS = [
    'source/**/*.pug',
    'source/**/_data.json',
    'source/_global_data.json'
];

gulp.task('css', function(callback) {
    pump([
        gulp.src(CSS_SOURCE_PATHS),
        gulp.dest(BUILD_DIR),
    ], callback);
});

gulp.task('jpg', function(callback) {
    pump([
        gulp.src(JPG_SOURCE_PATHS),
        gm(function(gmfile) {
            return gmfile
                .strip()
                .interlace('plane')
                .resize(1024, 1024, '@')
                .quality(75)
        }),
        gulp.dest(BUILD_DIR),
    ], callback);
});

gulp.task('png', function(callback) {
    pump([
        gulp.src(PNG_SOURCE_PATHS),
        imagemin(),
        gulp.dest(BUILD_DIR),
    ], callback);
});

const tsProject = ts.createProject('tsconfig.json');
gulp.task('typescript', function(callback) {
    pump([
        gulp.src(TYPESCRIPT_SOURCE_PATHS),
        tsProject(),
        gulp.dest(BUILD_DIR),
    ], callback);
});

function retrieveDataFromFile(file) {
    const dataPath = path.join(path.dirname(file.path), '_data.json');

    let dataString;
    try {
        dataString = fs.readFileSync(dataPath);
    } catch (ioError) {
        // Ignore all file read errors.
        return {};
    }

    try {
        const data = JSON.parse(dataString);
        const key = path.basename(file.path, '.pug');
        return key in data ? data[key] : {};
    } catch (parseError) {
        // Add the filename to the error message.
        parseError.message = `Failed to parse ${file.path}: ${parseError.message}`;
        throw parseError;
    }
}

function pathToUrl(p) {
    return p.split(path.sep).join('/');
}

function relativePath(a, b) {
    const r = path.relative(a, b);
    return (r != '' ? r : '.') + '/';
}

gulp.task('pug', function(callback) {
    pump([
        gulp.src(PUG_SOURCE_PATHS),
        data(function(file) {
            const rootToFilePath = file.relative.replace(/\.pug$/, '.html').replace(/index\.html$/, '');
            const rootToFolderPath = relativePath(file.base, path.dirname(file.path));
            const fileToRootPath = relativePath(path.dirname(file.path), file.base);
            return Object.assign({
                rootToFileUrl: pathToUrl(rootToFilePath),
                rootToFolderUrl: pathToUrl(rootToFolderPath),
                fileToRootUrl: pathToUrl(fileToRootPath),
            }, retrieveDataFromFile(file));
        }),
        gulpPug({
            locals: JSON.parse(fs.readFileSync('source/_global_data.json')),
            pug
        }),
        gulp.dest(BUILD_DIR),
    ], callback);
});

gulp.task('build', [
    'css',
    'jpg',
    'png',
    'pug',
    'typescript',
]);

gulp.task('clean', function() {
    return del(BUILD_DIR);
});

gulp.task('watch', ['build'], function() {
    gulp.watch(CSS_WATCH_PATHS, ['css']);
    gulp.watch(JPG_WATCH_PATHS, ['jpg']);
    gulp.watch(PNG_WATCH_PATHS, ['png']);
    gulp.watch(PUG_WATCH_PATHS, ['pug']);
    gulp.watch(TYPESCRIPT_WATCH_PATHS, ['typescript']);
});

gulp.task('default', ['build']);
