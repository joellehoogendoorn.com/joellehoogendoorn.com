# Development

During development, use `npm run watch` to continuously build the website. Any static file server can be used to then serve the `public/` folder. A development server can be started with `npm run serve`.
