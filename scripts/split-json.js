const fs = require('fs');

const inputPath = process.argv[2];

if (!inputPath) {
    console.error('Pass a json file to split');
    process.exit(1);
}

const data = JSON.parse(fs.readFileSync('_data.json'))

Object.keys(data).forEach(key => {
    fs.writeFileSync(key + '.json', JSON.stringify(data[key], null, 4));
})

fs.renameSync(inputPath, inputPath + '.split');
