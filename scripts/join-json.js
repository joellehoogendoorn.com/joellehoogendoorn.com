const fs = require('fs');

const outputPath = process.argv[2];

if (!outputPath) {
    console.error('Pass a file name to write to.')
    process.exit(1);
}

const data = fs.readdirSync('.')
      .filter(file => /\.json$/.test(file) && file != outputPath)
      .reduce((target, file) => {
          const data = JSON.parse(fs.readFileSync(file));
          target[file.replace(/\.json$/, '')] = data;
          return target;
      }, {})

fs.writeFileSync(outputPath, JSON.stringify(data, null, 4));

fs.readdirSync('.')
    .filter(file => /\.json$/.test(file) && file != outputPath)
    .forEach(file => fs.renameSync(file, file + '.joined'));
