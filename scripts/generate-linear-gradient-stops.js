// Configuration.
const options = {
    stops: 11, // At least two.
    maxOpacity: 1,
    precision: 4,
    approximationSections: 1000000
}

// Utility.
function sq(x) {
    return x*x;
}

const sqrt = Math.sqrt;

const sin = Math.sin;

const cos = Math.cos;

const asin = Math.asin;

const acos = Math.acos;

const PI = Math.PI;

function createLinearSpace(min, max, length) {
    const scale = (max - min)/(length - 1);
    const space = new Array(length);
    for (let i = 0; i < length; i++) {
        space[i] = min + scale*i;
    }
    return space;
}

// Application.
console.log([
    { name: 'circle-out-x-spaced', stops: createLinearSpace(0, 1, options.stops).map(x => [ x, 1 - sqrt(1 - sq(x - 1)) ]) },
    { name: 'circle-out-y-spaced', stops: createLinearSpace(1, 0, options.stops).map(y => [ 1 - sqrt(1 - sq(y - 1)), y ]) },
    { name: 'circle-out-arc-spaced', stops: createLinearSpace(0, PI/2, options.stops).map(x => [ 1 - cos(x), 1 - sin(x) ]) },
    { name: 'sine-out-x-spaced', stops: createLinearSpace(0, 1, options.stops).map(x => [ x, 1 - sin(PI/2*x) ]) },
    { name: 'sine-out-y-spaced', stops: createLinearSpace(1, 0, options.stops).map(y => [ asin(1 - y)*2/PI, y ]) },
    { name: 'sine-out-arc-spaced', stops: approximateArcSpacedStops(x => 1 - sin(PI/2*x)) },
    { name: 'quad-out-x-spaced', stops: createLinearSpace(0, 1, options.stops).map(x => [ x, sq(x - 1) ]) },
    { name: 'quad-out-y-spaced', stops: createLinearSpace(1, 0, options.stops).map(y => [ 1 - sqrt(y), y ]) },
    { name: 'quad-out-arc-spaced', stops: approximateArcSpacedStops(x => sq(x - 1)) },
].map(({ name, stops }) => {
    return `.${name} {\n`
        + '    background: linear-gradient(\n'
        + stops.map(([ position, opacity ]) => `        rgba(0, 0, 0, ${Number(opacity.toFixed(4))}) ${Number((100*position).toFixed(4))}%`).join(',\n') + '\n'
        + '    );\n'
        + '}\n'
}).join('\n'))

function approximateArcLength(f) {
    const sections = options.approximationSections;
    const dx = 1.0/sections;
    const dxsq = dx*dx;
    let y1 = f(0.0);
    let sum = 0.0;
    for (let i = 1; i <= sections; i++) {
        const y2 = f(i/sections);
        const dy = y2 - y1;
        sum += Math.sqrt(dxsq + dy*dy);
        y1 = y2;
    }
    return sum;
}

function approximateArcSpacedStops(f) {
    const l = approximateArcLength(f)
    const sections = options.approximationSections;
    const dx = 1.0/sections;
    const dxsq = dx*dx;
    let x1 = 0.0;
    let y1 = f(x1);
    let l1 = 0.0;
    const stops = [];
    let stop_index = 0;
    for (let i = 1; i <= sections; i++) {
        const x2 = i/sections;
        const y2 = f(x2);
        const dy = y2 - y1;
        const dl = Math.sqrt(dxsq + dy*dy);
        const l2 = l1 + dl;
        
        do {
            const ls = l*(stop_index/options.stops);
            if (ls >= l2) break;
            const fraction = (ls - l1)/(l2 - l1);
            stops[stop_index++] = [
                x1 + fraction*dx,
                y1 + fraction*dy,
            ];
        } while (true);
        x1 = x2;
        y1 = y2;
        l1 = l2;
    }

    stops[stop_index] = [x1, y1];
    return stops
}

function approximateError(f, stops) {
    const sections = options.approximationSections;
    let err = 0.0;
    let stopIndex = 0;
    for (let i = 0; i <= sections; i++) {
        const x = i/sections;
        const y = f(x);
        let [ x1, y1 ] = stops[stopIndex];
        let [ x2, y2 ] = stops[stopIndex + 1];
        if (x > x2) {
            stopIndex++;
            [ x1, y1 ] = stops[stopIndex];
            [ x2, y2 ] = stops[stopIndex + 1];
        }
        const ys = (x - x1)/(x2 - x1)*(y2 - y1) + y1;
        err += y - ys;
    }
    return err/sections;
}
