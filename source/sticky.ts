/*

Sticky means it stays on the viewport edge where it attaches, within the
constraints of its parent. If the height is larger than the viewport a scroll
bar should appear so you can see the bottom part of the box.

Another idea is to have the viewport edges move the box, within the
constraints of its parent. When the height is larger than the viewport you
must scroll down the entire page before being able to see the bottom part of
the box.

Another idea is basically parallax scrolling. The relative position of the
viewport to the parent is transfered to the relative position of the box to
the parent.

*/

document.addEventListener('DOMContentLoaded', function() {
    const sticky = document.getElementById('layout-side').firstElementChild as HTMLElement;
    const stickyParent = sticky.parentElement;
    const placeholder = document.createElement('div');
    {
        const stickyBounds = sticky.getBoundingClientRect();
        placeholder.style.width = stickyBounds.width + 'px';
        placeholder.style.height = stickyBounds.height + 'px';
        stickyParent.insertBefore(placeholder, sticky);
    }
    sticky.style.position = 'absolute';
    sticky.style.top = '0';
    sticky.style.bottom = 'auto';
    stickyParent.style.position = 'relative';
    stickyParent.style.top = '0';
    stickyParent.style.bottom = '0';


    window.addEventListener('scroll', requestUpdate);
    window.addEventListener('resize', requestUpdate);

    let pendingUpdate = false;
    function requestUpdate() {
        if (pendingUpdate) return;
        window.requestAnimationFrame(update);
    }

    const ABSOLUTE_BOTTOM = 0;
    const FIXED_TOP = 1;
    const ABSOLUTE = 2;
    const FIXED_BOTTOM = 3;
    const ABSOLUTE_TOP = 4;

    let state = ABSOLUTE;

    let lastScrollY = window.scrollY;
    let lastWindowHeight = window.innerHeight;
    let lastStickyBounds = sticky.getBoundingClientRect();
    let lastParentBounds = stickyParent.getBoundingClientRect();
    function update() {
        const scrollY = window.scrollY;
        const windowHeight = window.innerHeight;
        const stickyBounds = sticky.getBoundingClientRect();
        const parentBounds = stickyParent.getBoundingClientRect();

        // Assume box height <= viewport height. for now. FIXME
        switch (state) {
            case ABSOLUTE: {
                // Transition to fixed if box out of view except when it doesnt fit in parent.
                if (stickyBounds.top < 0) {
                    // Does the box stay within the bounds of its parent when we would fix it?
                    if (stickyBounds.height < parentBounds.bottom) {
                        sticky.style.position = 'fixed';
                        sticky.style.top = '0px';
                        sticky.style.bottom = 'auto';
                        state = FIXED_TOP;
                    }
                    // Else move it to the bottom of the parent.
                    else {
                        // TODO: This should already be the case, check it. Not true, can be at 99 in absolute, scroll to 100 which is also absolute.
                        // TODO: Add ABSOLUTE_BOTTOM and ABSOLUTE_TOP
                        // sticky.style.position = 'absolute';
                        sticky.style.top = 'auto';
                        sticky.style.bottom = '0px';
                        // state = ABSOLUTE;
                    }
                }
                // viewport bottom < box bottom.
                else if (stickyBounds.bottom > windowHeight) {
                    console.log(stickyBounds, windowHeight);
                    // If the moved box top > parent top, move it up.
                    // (newScrollY + windowHeight - stickyBounds.height) < (newScrollY + parentBounds.top)
                    if (windowHeight - stickyBounds.height > parentBounds.top) {
                        // (newScrollY + parentBounds.top) + top + stickyBounds.height == newScrollY + window.innerHeight
                        // top = windowHeight - stickyBounds.height - parentBounds.height;
                        sticky.style.position = 'fixed';
                        sticky.style.top = 'auto'
                        sticky.style.bottom = '0px';
                        state = FIXED_BOTTOM;
                    }
                    // Else move it to the top of the parent.
                    else {
                        // sticky.style.position = 'absolute';
                        sticky.style.top = '0px';
                        sticky.style.bottom = 'auto';
                        // state = ABSOLUTE;
                    }
                }
                break;
            }
            case FIXED_TOP: {
                // If viewport bottom < last viewport bottom
                const viewportTopDelta = scrollY - lastScrollY;
                if (viewportTopDelta < 0) {
                    // Make absolute.
                    sticky.style.position = 'absolute';
                    sticky.style.top = (lastStickyBounds.top - lastParentBounds.top) + 'px';
                    // sticky.style.bottom = 'auto';
                    state = ABSOLUTE;
                } else if (viewportTopDelta > 0) {
                    // Make absolute if crossing parent bounds.
                    if (stickyBounds.bottom > parentBounds.bottom) {
                        sticky.style.position = 'absolute';
                        sticky.style.top = 'auto'
                        sticky.style.bottom = '0px';
                        state = ABSOLUTE;
                    }
                } else {
                    // Nothing to do, unless parent changed position/size.
                }
                // const viewportBottomDelta = (scrollY + windowHeight) - (lastScrollY + lastWindowHeight);
                break;
            }
            case FIXED_BOTTOM: {
                // If viewport bottom < last viewport bottom
                const viewportBottomDelta = (scrollY + windowHeight) - (lastScrollY + lastWindowHeight);
                if (viewportBottomDelta > 0) {
                    // Make absolute.
                    sticky.style.position = 'absolute';
                    sticky.style.top = (lastStickyBounds.top - lastParentBounds.top) + 'px';
                    sticky.style.bottom = 'auto';
                    state = ABSOLUTE;
                } else if (viewportBottomDelta < 0) {
                    // Make absolute if crossing parent bounds.
                    if (stickyBounds.top < parentBounds.top) {
                        sticky.style.position = 'absolute';
                        sticky.style.top = '0px';
                        sticky.style.bottom = 'auto';
                        state = ABSOLUTE;
                    }
                } else {
                    // Nothing to do, unless parent changed position/size.
                }
                break;
            }
        }

        lastScrollY = scrollY;
        lastWindowHeight = windowHeight;
        lastStickyBounds = stickyBounds;
        lastParentBounds = parentBounds;
    }
});
