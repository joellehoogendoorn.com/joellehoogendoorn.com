Array.from(document.getElementsByClassName('hor-scroll-ind-container'))
.forEach(function(container) {
    const left = container.querySelector('.hor-scroll-ind-left') as HTMLElement;
    const right = container.querySelector('.hor-scroll-ind-right') as HTMLElement;
    const viewport = container.querySelector('.hor-scroll-ind-viewport') as HTMLElement;

    function update() {
        // Calculate the space taken by the scrollbar and add it to bottom.
        const bottom = (viewport.offsetHeight - viewport.clientHeight) + 'px';
        left.style.opacity = viewport.scrollLeft > 0 ? '1' : '0';
        left.style.bottom = bottom;
        right.style.opacity = viewport.scrollLeft < viewport.scrollWidth - viewport.clientWidth ? '1' : '0';
        right.style.bottom = bottom;
    };

    let updatePending = false;
    function requestUpdate() {
        if (updatePending) return
        updatePending = true;
        requestAnimationFrame(function() {
            update();
            updatePending = false;
        })
    }

    requestUpdate();
    viewport.addEventListener('scroll', requestUpdate);
    window.addEventListener('resize', requestUpdate);
});

Array.from(document.getElementsByClassName('header-image'))
.forEach(function (element: HTMLElement) {
    const url = element.getAttribute('data-header-image-url')
    const img = new Image();
    img.addEventListener('load', () => {
        element.style.backgroundImage = `url(${url})`;
        element.style.opacity = '1';
    })
    img.src = url;
});